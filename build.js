/**
 * This file has been changed to include a MathJax config file into the bundle
 * and to only make the desired build configuration.
 */
var build = require('./main.js').build;

build(
    'TeX',
    'TeX',
    'SVG', {
        toFile: true,
        compress: true,
        customExtensions: [
            "/extensions/tex2jax.js",
            "/extensions/Safe.js",
            "/extensions/TeX/AMSmath.js",
            "/extensions/TeX/AMSsymbols.js",
            "/extensions/TeX/noErrors.js",
            "/extensions/TeX/noUndefined.js",
            '/extensions/TeX/cancel.js',
            '/extensions/TeX/mathchoice.js',
            '/extensions/TeX/mediawiki-texvc.js',
            '/extensions/TeX/mhchem.js',
            '/extensions/TeX/newcommand.js',
            '/extensions/TeX/unicode.js',
            '/extensions/TeX/verb.js',
        ],
        webfontURL: ' ',
        configPath: 'mathseer-config.js',
    });